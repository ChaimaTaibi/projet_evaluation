#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#
# Exercice : Projet_evaluation_shiny
# Objetifs :
# -creation dashbord
#
# Date de création : 01/01/2024
# Date de dernière modification :19/01/2024
# Auteur : TAIBI Chaima

## Chargement des packages
library(shiny)
library(shinydashboard)

#Chargement des données:

rea_ped_medicament_mesure <- read.csv('C:/Users/ilis-data-science-7/Desktop/Lamer/lamer csv/rea_ped_medicament_mesure.csv') 
rea_ped_medicament_mesure


# UI
ui <- dashboardPage(
  dashboardHeader(title = "Tableau de Bord Patient"),
  dashboardSidebar(),
  dashboardBody(
    tabItem(
      tabName = "patient",
      fluidRow(
        box(
          title = "Nombre de Patients",
          status = "info",
          numericInput("patientCount", "Sélectionnez le nombre de patients :", 4, min = 1, max = 100)
        ),
        box(
          title = "Résumé des Informations",
          status = "primary",
          tableOutput("patientSummary"),
          plotOutput("ageHistogram")
        ),
        box(
          title = "Insuffisance Cardiaque",
          status = "warning",
          plotOutput("insuffisanceCardiaquePlot")
        ),
        box(
          title = "Répartition par Sexe",
          status = "success",
          plotOutput("sexeDistributionPlot")
        )
      )
    )
  )
)

# Serveur
server <- function(input, output) {
  # Résumé des informations
  output$patientSummary <- renderTable({
    # Sélectionner uniquement les colonnes nécessaires (ajustez selon votre dataset)
    selected_columns <- c("age", "poids", "sexe", "insuffisance_cardiaque")
    
    # Vérifier si le dataset rea_ped_medicament_mesure existe et n'est pas vide
    if (!exists("rea_ped_medicament_mesure") || nrow(rea_ped_medicament_mesure) == 0) {
      return(NULL)
    }
    
    # Sélectionner les premières lignes en fonction du nombre de patients choisi
    result_df <- head(rea_ped_medicament_mesure[, selected_columns], n = input$patientCount)
    return(result_df)
  })
  
  # Histogramme de l'âge
  output$ageHistogram <- renderPlot({
    # Vérifier si le dataset rea_ped_medicament_mesure existe et n'est pas vide
    if (!exists("rea_ped_medicament_mesure") || nrow(rea_ped_medicament_mesure) == 0) {
      return(NULL)
    }
    
    # Tracer l'histogramme de l'âge
    hist(rea_ped_medicament_mesure$age, main = "Histogramme de l'âge", xlab = "Âge", col = "skyblue", border = "black")
  })
  
  # Graphique de l'insuffisance cardiaque
  output$insuffisanceCardiaquePlot <- renderPlot({
    # Vérifier si le dataset rea_ped_medicament_mesure existe et n'est pas vide
    if (!exists("rea_ped_medicament_mesure") || nrow(rea_ped_medicament_mesure) == 0) {
      return(NULL)
    }
    
    # Tracer le graphique en barres pour l'insuffisance cardiaque
    barplot(table(rea_ped_medicament_mesure$insuffisance_cardiaque),
            main = "Distribution de l'Insuffisance Cardiaque",
            xlab = "Insuffisance Cardiaque",
            col = c("skyblue", "salmon"),
            legend.text = TRUE)
  })
  
  # Graphique de la répartition par sexe
  output$sexeDistributionPlot <- renderPlot({
    # Vérifier si le dataset rea_ped_medicament_mesure existe et n'est pas vide
    if (!exists("rea_ped_medicament_mesure") || nrow(rea_ped_medicament_mesure) == 0) {
      return(NULL)
    }
    
    # Tracer le graphique en barres pour la répartition par sexe
    barplot(table(rea_ped_medicament_mesure$sexe),
            main = "Répartition par Sexe",
            xlab = "Sexe",
            col = c("skyblue", "pink"),
            legend.text = TRUE)
  })
}

# Lancer l'application Shiny
shinyApp(ui, server)
